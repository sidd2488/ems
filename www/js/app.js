// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'starter.remoteService', 'starter.login', 'starter.studentDetailsService', 'starter.studentDetails','starter.timetableService', 'starter.timetable', 'starter.noticeboardService', 'starter.noticeboard'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    document.addEventListener("deviceready", onDeviceReady, false);
			function onDeviceReady() {
			if(window.plugin != undefined){
			var notificationOpenedCallback = function(jsonData) {
			  alert("Notification opened:\n" + JSON.stringify(jsonData));
			  console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
			};

			window.plugins.OneSignal
			  .startInit("AIzaSyBAvM31xHMtgjqjHp8-b0N2n08dguAnrJ8", "141054021841")
			  .handleNotificationOpened(notificationOpenedCallback)
			  .endInit();

	 }}
   });
 })

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.timetable', {
    url: '/timetable',
    views: {
      'menuContent': {
        templateUrl: 'templates/timetable.html',
		controller: 'timetableCtrl as timetableCtrl'
      }
    }
  })
  .state('app.student-update-information', {
    url: '/student-update-information/:userid',
    views: {
      'menuContent': {
        templateUrl: 'templates/student-update-information.html',
		controller:'studentProfileCtrl as studentProfileCtrl'
      }
    }
  })
  .state('app.student-details', {
    url: '/student-details/:userid',
    views: {
      'menuContent': {
        templateUrl: 'templates/student-details.html',
		controller:'studentDetailsItemCtrl as studentDetailsCtrl'
      }
    }
  })
  .state('app.notice-board', {
    url: '/notice-board',
    views: {
      'menuContent': {
        templateUrl: 'templates/notice-board.html',
		controller: 'noticeboardCtrl as noticeboardCtrl'
      }
    }
  })
  .state('app.exam-timetable', {
    url: '/exam-timetable',
    views: {
      'menuContent': {
        templateUrl: 'templates/exam-timetable.html'
      }
    }
  })

  .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html'
        }
      }
    })
  
  .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
		  controller: 'LoginController as loginCtrl'
        }
      }
    })
    .state('app.complain', {
      url: '/complain',
      views: {
        'menuContent': {
          templateUrl: 'templates/complain.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.attendance', {
    url: '/attendance',
    views: {
      'menuContent': {
        templateUrl: 'templates/attendance.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
