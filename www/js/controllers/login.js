/**
 * Login controller
 */
angular
	.module('starter.login', [])

	.controller('LoginController',
		function ($ionicSideMenuDelegate,$state,$scope,$ionicPopup,$timeout,$window,$cordovaDevice,remoteService) {
		'use strict';
		$ionicSideMenuDelegate.canDragContent(false);
		var vm = this;
        vm.doLogin = doLogin;
	  /***********************************************************/
      /***********************************************************/
		function doLogin(data) {
		  var email = data.email;
		  var password = data.password;
		  var role = data.role;
		  remoteService.login(email, password, role).then(function (res) {  
		  alert(res.data.message);
		  if(res.data=="successful"){
		  window.localStorage.setItem("userid", data.email);
		  $state.go('app.home', {}, {location: "replace", reload: true});
		  console.log('Login response: ' + JSON.stringify(res));
		  }
		}, function (err) {
		 console.error('Error in login: ' + JSON.stringify(err));
		});
	 }
});
