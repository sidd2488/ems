/**
 * Created by Siddhartha Kulshreshtha on 19/03/2018.
 */
 angular
  .module('starter.noticeboard', [])
  .controller('noticeboardCtrl',function ($http,$scope,$state,$state,$timeout,$ionicLoading,noticeboardService) {
	 // Setup the loader
     $ionicLoading.show({
     content: 'Loading',
     animation: 'fade-in',
     showBackdrop: true,
     maxWidth: 200,
     showDelay: 0
     });
	 $timeout(function () {
     $ionicLoading.hide(); 									  
	 $scope.arr=[];
	 function getNoticeboard(){
     noticeboardService.getNoticeboard()
		  .then(function (res) {
		     $scope.arr=res.data;
			 console.log(res.data);
		  }, function (err) {
		  console.error(err);
	   	 });
	  }
	  getNoticeboard();
	 }, 2000);
  });
    

