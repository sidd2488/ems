/**
 * Application controller
 */
(function() {
angular
	.module('invisionApp')
	.controller('ApplicationController', [
		'$scope',
		'$ionicModal',
		'$state',
        '$window',
        '$ionicHistory',
        '$ionicLoading',
        '$timeout',
		function ($scope, $ionicModal, $state, $window, $ionicHistory, $ionicLoading, $timeout) {
		    'use strict';
			var vm = this;
			vm.showMenu = true;
			$scope.$on('hideMenu', function() {
				vm.showMenu = false;
			});
			$scope.$on('$stateChangeStart', function () {
				vm.showMenu = true;
			});
            /*********************************************************/
			/********************************************************/
			function showModal(modal) {
				$scope.modal = modal;
				// Open the intro modal
				$scope.modal.show();
			}
           /*********************************************************/
		   /********************************************************/
			// Triggered in the intro modal to open it
			vm.openModal = openModal;
            /*********************************************************/
			/********************************************************/
			// Triggered in the intro modal to close it
			vm.closeModal = closeModal;
			/*********************************************************/
			/********************************************************/
			// Create the intro modal that we will use later
			function openModal() {
				$ionicModal.fromTemplateUrl('templates/modal.html', {
					scope: $scope
				}).then(showModal);
			}
			function closeModal() {
				$scope.modal.hide();
			}
			$ionicModal.fromTemplateUrl('templates/datestartmodal.html', 
            function(modal) {
            $scope.datestartmodal = modal;
            },
            {
			/*********************************************************/
			/********************************************************/
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope, 
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
            }
            );
			$ionicModal.fromTemplateUrl('templates/dateendmodal.html', 
            function(modal) {
            $scope.dateendmodal = modal;
            },
            {
			/*********************************************************/
			/********************************************************/
            // Use our scope for the scope of the modal to keep it simple
            scope: $scope, 
            // The animation we want to use for the modal entrance
            animation: 'slide-in-up'
            }
            );
           $scope.openstartdateModal = function() {
           $scope.datestartmodal.show();
           };
		   $scope.data={};
           $scope.closestartdateModal = function(modal) {
           $scope.datestartmodal.hide();
           $scope.data.start_date = modal;
           };
		   /********************************************************/
		   /********************************************************/
		   $scope.openenddateModal = function() {
           $scope.dateendmodal.show();
           };
           $scope.closeenddateModal = function(modal) {
           $scope.dateendmodal.hide();
           $scope.data.end_date = modal;
           };
		   /**************************************************************/
                    /*Footer Menubar Hide JS CODE*/
           /**************************************************************/
			var vm2 = this;
			vm2.showFooterMenu = false;
			$scope.$on('$stateChangeStart', function () {
				if ($state.current.name == 'app.login' || $state.current.name == 'app.register' || $state.current.name == 'app.forget-password')
				{
					vm2.showFooterMenu = false;
				} else
				{
					vm2.showFooterMenu = true;
				}
			});
		   $scope.logout = function ()
            {
			$timeout(function () {
				$ionicLoading.hide();
				$ionicHistory.clearCache();
				$window.localStorage.clear();
				$ionicHistory.clearHistory();
				$ionicHistory.nextViewOptions({disableBack: true, historyRoot: true});
				$state.go('app.login');
			}, 2000);
          };
	   }
	]);
})();
