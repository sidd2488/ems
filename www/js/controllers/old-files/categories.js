/**
 * Created by Siddhartha Kulshreshtha on 10/4/17.
 */
angular
  .module('invisionApp.categories', [])
  .controller('EventMapCtrl',function ($http,$scope,$state,$window,categoryService,$ionicLoading) {
   google.maps.event.addDomListener(window, 'load', function() {
		var geocoder = new google.maps.Geocoder(); // initialize google map object
        var address = $state.params.locations;
        geocoder.geocode( { 'address': address}, function(results, status) {
	    var latitude = results[0].geometry.location.lat();
	    var longitude = results[0].geometry.location.lng();
        var myLatlng = new google.maps.LatLng(latitude,longitude);
        var mapOptions = {
            center: myLatlng,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        navigator.geolocation.getCurrentPosition(function(pos) {
            map.setCenter(new google.maps.LatLng(latitude, longitude));
            var myLocation = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longitude),
                map: map,
                title: "My Location"
            });
        });
        $scope.map = map;
	   })
    });
})
   /***********************************************************/
   /***********************************************************/ 
.controller('EventDetailsCtrl',function ($http,$scope,$state,$ionicPopup,$timeout,$window,categoryService) {
	
	$scope.getCommentEvent=function(eventid)
	{	 
	   $state.go('app.comments', {"eventId": eventid});
	}									 
   /***********************************************************/
   /***********************************************************/ 									 
	   $scope.showAlert = function (title, msg) {
                var alertPopup = $ionicPopup.alert({
                    title: title,
                    template: msg
                });
                alertPopup.then(function (res) {
                    $timeout(function () {
                        $window.location.reload(true);
                    });
                });
            };
   /***********************************************************/
   /*booking event js code*/
   /***********************************************************/		
	$scope.getEventBooking = function(){
	  var event_id=$state.params.eventId;
	  var user_id=window.localStorage.getItem("userid");
	  categoryService.getEventBooking(event_id,user_id)
	  .then(function (res) {
		if(res.data=="successful")
         {
          $scope.showAlert("Success", "<style>.popup {background-color:#f96332!important;} .popup-body p{color:#fff !important} .popup-head h3{color:#fff !important} .button{background:#fff !important; color:#000 !important}</style><p>Event Booking Successful!<p/>");
         } 
	  }, function (err) {
	  console.error(err);
	  });
	}	
	/*****************************************************************/
	/**Fetching The Events List By Category Id On Category.html Page**/
	/*****************************************************************/
	$scope.getIndividualEventbylocation=function(locations)
	{	 
	   $state.go('app.event-map-location', {"locations": locations});
	   setInterval(function(){ $window.location.reload(true); }, 3000);
	}		  
	$scope.arr2=[];
	var eventById=$state.params.eventId;
	categoryService.getIndividualEventbyid(eventById)
	.then(function (res) {	 
	   $scope.arr2 = res.data;
       console.log(res.data);
       }, function (err) {
        console.error(err);
    });
})
  
.controller('CategoriesItemCtrl',function ($http,$scope,$state,categoryService) {
	/*****************************************************************/
	/**Fetching The Events List By Category Id On Category.html Page**/
	/*****************************************************************/
	$scope.getIndividualEventbyid=function(eventid)
	{	 
	   $state.go('app.detail-event', {"eventId": eventid});
	}	
	$scope.arr1=[];
	var eventCategoryid=$state.params.categoryId;
	categoryService.getEventbyid(eventCategoryid)
	.then(function (res) {	 
	   $scope.arr1 = res.data;
       console.log(res.data);
       }, function (err) {
        console.error(err);
   });
})
  
.controller('CategoriesCtrl',function ($http,$scope,$state,categoryService) {
	 /***************************************************************/
	 /**Fetching The Category List On Categories.html Page**/
	 /***************************************************************/
	 $scope.getEventbyid=function(eventCategoryid)
	 {	 
	   $state.go('app.category', {"categoryId": eventCategoryid});
	 }
	 $scope.arr=[];
	 function getCategorylist(){
     categoryService.getCategories()
		  .then(function (res) {
		  $scope.arr = res.data;
		  console.log(res.data);
		  }, function (err) {
		  console.error(err);
	   	 });
	 }	 
	 getCategorylist();
  });