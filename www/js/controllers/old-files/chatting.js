/**
 * Created by Siddhartha Kulshreshtha on 10/4/17.
 */
 angular
  .module('invisionApp.chatting', ['ionic'])
  .controller('chattingController',function ($http,$scope,$state,$window,chattingService) {
     'use strict';
	 /***********************************************************/
	 /*profile update js code*/
	 /***********************************************************/      
      var vm = this;
	  vm.addChat = addChat;
	  function addChat(data) {
		 chattingService.addChat(data.message)
		.then(function (res) {
		console.log(res);
		$state.go('app.chatting');
		$window.location.reload();
		}, function (err) {
		console.error(err);
	  });
     }
	/***************************************************************/
	 /**Fetching User Data On Profile.html Page**/
	 /***************************************************************/
	 $scope.messages =[];
	 $scope.sender_ids=window.localStorage.getItem("userid");
	 function getChat(){
	 $scope.userid=window.localStorage.getItem("userid");
     chattingService.getChat($scope.userid)
		  .then(function (res) {
				$scope.messages=res.data;
				//setInterval(function(){ $window.location.reload(true); }, 5000);
				console.log(res.data);
		  }, function (err) {
		  console.error(err);
	   	 })
	  }
	  getChat();
  });
    

