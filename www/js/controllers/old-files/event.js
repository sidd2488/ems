/**
 * Created by Siddhartha Kulshreshtha on 10/4/17.
 */
 
angular
	.module('invisionApp.eventService1', ['ionic'])
	.controller('eventController',function ($scope,$ionicSideMenuDelegate,$state,$cordovaActionSheet, $cordovaCamera,$ionicPopup,$timeout,$window,eventService) {
		'use strict';
		$ionicSideMenuDelegate.canDragContent(false);
		var vm = this;
		vm.eventCreate = eventCreate;
		vm.eventComment = eventComment;
		vm.commentByEventId = commentByEventId;
		$scope.arr1=[];
		function commentByEventId(){
		  var eventId=$state.params.eventId;
	      eventService.commentByEventId(eventId)
		 .then(function (res) {
		  $scope.arr1 = res.data;
	      }, function (err) {
		  console.error(err);
	     });
	   }
	   commentByEventId();
		/***********************************************************/
		/***********************************************************/
		function eventComment(data) {
		  var eventId=$state.params.eventId;
		  eventService.eventComment(eventId,data.comment)
		 .then(function (res) {
		 if(res.data=="successful")
         {
          $scope.showAlert("Success", "<style>.popup {background-color:#f96332!important;} .popup-body p{color:#fff !important} .popup-head h3{color:#fff !important} .button{background:#fff !important; color:#000 !important}</style><p>Event Comment Saved Successful!<p/>");
         }
		 if(res.data=="again")
		 {
		  $scope.showAlert("Success", "<style>.popup {background-color:#ccc!important;} .popup-body p{color:#000 !important} .popup-head h3{color:#fff !important} .button{background:#fff !important; color:#000 !important}</style><p>User Have Already Comment On This Event!<p/>");	  
		 }
		 }, function (err) {
		 console.error('Error in event comment: ' + JSON.stringify(err));
	     });
		}
		/***********************************************************/
		/***********************************************************/
		 //File Upload Code Starts Here
            $scope.image = null;
            $scope.URL = null;
            $scope.editimage = "img/user.svg";
            $scope.input = true;
            $scope.takepic = function () {
                var actionSheetOptions = {
                    title: 'Select a picture',
                    buttonLabels: ['Camera', 'Choose from gallery'],
                    addCancelButtonWithLabel: 'Cancel',
                    androidEnableCancelButton: true,
                };
                $cordovaActionSheet.show(actionSheetOptions).then(function (btnIndex) {
                    var index = btnIndex;
                    if (index == 2) {
                        $scope.cameraFunc(Camera.PictureSourceType.PHOTOLIBRARY)
                    } else if (index == 1) {
                        $scope.cameraFunc(Camera.PictureSourceType.CAMERA)
                    }
                });
            };
			/***********************************************************/
			/***********************************************************/
            $scope.cameraFunc = function (picType) {
                var options = {
                    quality: 50,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: picType,
                    allowEdit: false,
                    encodingType: 0,
                    targetWidth: 600,
                    targetHeight: 500,
                    encodingType: Camera.EncodingType.JPEG,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false,
                    correctOrientation: true
                }
                $cordovaCamera.getPicture(options).then(function (imageData) {
                    $scope.editimage = "data:image/jpeg;base64," + imageData;
                    $scope.URL = imageData;
                }, function (err) {
                    console.log(JSON.stringify(err));
                });
            };
            /***********************************************************/
            /***********************************************************/
            $scope.choosepic = function () {
                var options = {
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    destinationType: Camera.DestinationType.FILE_URI,
                    quality: 400,
                    targetWidth: 400,
                    targetHeight: 400,
                    encodingType: Camera.EncodingType.JPEG,
                    correctOrientation: true
                };
                $cordovaCamera.getPicture(options).then(function (imageURI) {
                    // var image = document.getElementById('myImage');
                    $scope.editimage = "data:image/jpeg;base64," + imageData;
                    $scope.imgURI = "data:image/jpeg;base64," + imageURI;
                    // $scope.image.push($scope.imgURI);
                    //  image.src = imageURI;
                }, function (err) {
                    // error
                });
            };	
		/***********************************************************/
		/*create events js code*/
		/***********************************************************/
		function eventCreate(data) {
		 eventService.eventCreate(data.category_id,data.event_title, data.location, data.start_date, data.end_date, $scope.URL, data.description)
		 .then(function (res) {
		 if(res.data=="successful")
         {
          $scope.showAlert("Success", "<style>.popup {background-color:#f96332!important;} .popup-body p{color:#fff !important} .popup-head h3{color:#fff !important} .button{background:#fff !important; color:#000 !important}</style><p>Event Saved Successful!<p/>");
         } 
		 }, function (err) {
		 console.error('Error in event create: ' + JSON.stringify(err));
	   });
       }
	   /***********************************************************/
	   /***********************************************************/
	   $scope.showAlert = function (title, msg) {
                var alertPopup = $ionicPopup.alert({
                    title: title,
                    template: msg
                });
                alertPopup.then(function (res) {
                    $timeout(function () {
                        $window.location.reload(true);
                    });
                });
            };
	   /***********************************************************/
	   /*fetch categories dropdown js code*/
       /***********************************************************/
	   $scope.arr=[];
	   function getCategoriesDropdown(){
	   eventService.getCategoriesDropdown()
		.then(function (res) {
		 $scope.arr = res.data;
	     }, function (err) {
		 console.error(err);
	    });
	   }	 
	 getCategoriesDropdown();   
});

