/**
 * Created by Siddhartha Kulshreshtha on 10/4/17.
 */
angular
  .module('starter.studentDetails', [])	
  
   .controller('studentProfileCtrl',function ($http,$scope,$window,$state,$timeout, $ionicLoading,studentDetailsService) {										  
	// Setup the loader
    $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
    });
	$timeout(function () {
    $ionicLoading.hide();
	/***********************************************/
	$scope.data = {};
	var studentId= window.localStorage.getItem("userid");
	studentDetailsService.getstudentbyid(studentId)
	.then(function (res) {	 
	   $scope.data.stu_name = res.data['stu_name'];
	    $scope.data.dob = res.data['dob'];
	   $scope.data.perm_address = res.data['perm_address'];
	   $scope.data.temp_address = res.data['temp_address'];
	   $scope.data.email = res.data['email'];
	   $scope.data.contact_no = res.data['contact_no'];
	   $scope.data.father_name = res.data['father_name'];
	   $scope.data.father_occupation = res.data['father_occupation'];
	   $scope.data.father_qualification = res.data['father_qualification'];
	   $scope.data.mother_name = res.data['mother_name'];
	   $scope.data.mother_occupation = res.data['mother_occupation'];
	   $scope.data.mother_qualification = res.data['mother_qualification'];
       console.log(res.data);
       }, function (err) {
        console.error(err);
   });
   }, 2000);
 })
  .controller('studentDetailsItemCtrl',function ($http,$scope,$state,$timeout,$ionicLoading,studentDetailsService) {
	$ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
    });
	$timeout(function () {
    $ionicLoading.hide();
	$scope.arr1=[];
	var studentId=window.localStorage.getItem("userid");
	studentDetailsService.getstudentbyid(studentId)
	.then(function (res) {	 
	   $scope.arr1 = res.data;
	   var name = angular.element(document.querySelector('#name'));
	   var standard = angular.element(document.querySelector('#class'));
	   var section = angular.element(document.querySelector('#section'));
	   var year = angular.element(document.querySelector('#year'));
	   var father_name = angular.element(document.querySelector('#father_name'));
	   var father_occupation = angular.element(document.querySelector('#father_occupation'));
	   var father_education = angular.element(document.querySelector('#father_education'));
	   var mother_name = angular.element(document.querySelector('#mother_name'));
	   var mother_occupation = angular.element(document.querySelector('#mother_occupation'));
	   var mother_education = angular.element(document.querySelector('#mother_education'));
	   var permanent_address = angular.element(document.querySelector('#permanent_address'));
	   var temporary_address = angular.element(document.querySelector('#temporary_address'));
	   var contact = angular.element(document.querySelector('#contact'));
	   var email_id = angular.element(document.querySelector('#email_id'));
	   name.append(res.data.stu_name);
	   standard.append(res.data.class);
	   section.append(res.data.section);
	   year.append(res.data.Session);
	   permanent_address.append(res.data.perm_address);
	   temporary_address.append(res.data.temp_address);
	   father_name.append(res.data.father_name);
	   father_occupation.append(res.data.father_occupation);
	   father_education.append(res.data.father_education);
	   mother_name.append(res.data.mother_name);
	   mother_occupation.append(res.data.mother_occupation);
	   mother_education.append(res.data.mother_education);
	   contact.append(res.data.contact_no);
	   email_id.append(res.data.email);
       console.log(res.data);
       }, function (err) {
        console.error(err);
   });
	}, 2000);
  })