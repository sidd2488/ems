/**
 * Created by Siddhartha Kulshreshtha on 19/03/2018.
 */
 angular
  .module('starter.timetable', [])
  .controller('timetableCtrl',function ($http,$scope,$state,$timeout,$ionicLoading,timetableService) {
	// Setup the loader
    $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
    });
	$timeout(function () {
    $ionicLoading.hide();
	 $scope.arr=[];
	 function getTimetable(){
     timetableService.getClassTimetabled()
		  .then(function (res) {
		     $scope.arr=res.data;
			 console.log(res.data);
		  }, function (err) {
		  console.error(err);
	   	 });
	  }
	  getTimetable();
	 }, 2000);
  });
    

