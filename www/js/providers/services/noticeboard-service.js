/**
 * Created by Siddhartha Kulshreshtha on 10/4/17.
 */
angular
  .module('starter.noticeboardService', [])
  .service('noticeboardService', function ($http) {
    'use strict';
	 var vm = this;
	 /***********************************************************/
	/*fetch timetable data js code*/
	 /***********************************************************/
	vm.noticeboardfetchURL = 'http://localhost/ems/ems-app/users.php';
	function _getNoticeboard(){
		return $http.get(vm.noticeboardfetchURL+"?action="+"getNoticeboard");
	};
	
    return {
     getNoticeboard:_getNoticeboard
    };
  });
