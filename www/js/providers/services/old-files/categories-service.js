/**
 * Created by Siddhartha Kulshreshtha on 10/4/17.
 */
angular
  .module('invisionApp.categoryService', [])
  .service('categoryService', function ($http, routesConfig) {
    'use strict';
	var vm = this;
    vm.categoryURL = 'http://weirddcreations.com/app/categories.php';
	/***********************************************************/
	/*fetch categories js code*/
    /***********************************************************/
    function _getCategories() { 	
	   return $http.get(vm.categoryURL+"?action="+"getCategories");
    };
	/***********************************************************/
	/*fetch events js code*/
	 /***********************************************************/
	vm.eventfetchURL = 'http://weirddcreations.com/app/events.php';
	function _getEventbyid(eventCategoryid){
		return $http.get(vm.eventfetchURL+"?action="+"getEvents"+"&category_id="+eventCategoryid);
	};
	/***********************************************************/
	/*fetch single events js code*/
	/***********************************************************/
	function _getIndividualEventbyid(eventId){
		return $http.get(vm.eventfetchURL+"?action="+"getEventsById"+"&eventId="+eventId);
	};
	/***********************************************************/
	/*fetch single events map js code*/
	/***********************************************************/
	function _getIndividualEventbylocation(location){
		return location;
	};
	/***********************************************************/
	/*booking event js code*/
    /***********************************************************/
    function _getEventBooking(event_id,user_id) {
		var postData = {
		  event_id:event_id,
		  user_id:user_id,
		  action:'getEventBooking'
        };
		return $http.post(vm.eventfetchURL,postData);
    }
    return {
      getCategories: _getCategories,
	  getEventbyid:_getEventbyid,
	  getIndividualEventbyid:_getIndividualEventbyid,
	  getIndividualEventbylocation:_getIndividualEventbylocation,
	  getEventBooking:_getEventBooking
    };
  });
