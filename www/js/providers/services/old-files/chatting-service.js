/**
 * Created by Siddhartha Kulshreshtha on 10/4/17.
 */
angular
  .module('invisionApp.chattingService', [])
  .service('chattingService', function ($http, routesConfig) {
    'use strict';
	 var vm = this;
	 /***********************************************************/
	/*fetch chat data js code*/
	 /***********************************************************/
	vm.chatfetchURL = 'http://weirddcreations.com/app/chatting.php';
	function _getChat(userid){
		return $http.get(vm.chatfetchURL+"?action="+"getChat"+"&sender_id="+userid);
	};
	/***********************************************************/
	/*fetch add chat data js code*/
	/***********************************************************/
    function _addChat(message) {
		var user_id=window.localStorage.getItem("userid");
        var postData = {
          sender_id: user_id,
          receiver_id: "admin-1",
          message: message,
		  action:'userChat'
        };
		return $http.post(vm.chatfetchURL, postData);
      }
    return {
     addChat:_addChat,
	 getChat:_getChat
    };
  });
