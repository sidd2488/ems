/**
 * Created by Siddhartha Kulshreshtha on 10/4/17.
 */
angular
  .module("invisionApp.eventService", [])
  .service("eventService",
    function ($http) {
      'use strict';
      var vm = this;
	  vm.eventcommentURL1 = 'http://weirddcreations.com/app/event-comments.php';
	  function commentByEventId(event_id)
	  {
		return $http.get(vm.eventcommentURL1+"?action="+"getEventComments"+"&event_id="+event_id);
	  }
	  /***********************************************************/
	  /*event comment js code*/
      /***********************************************************/
	  vm.eventcommentURL = 'http://weirddcreations.com/app/event-comments.php';
	   function eventComment(event_id,comment) 
	   {
		  var user_id=window.localStorage.getItem("userid");
		  var postData = {
		  user_id:user_id,
		  event_id:event_id,
          comment: comment,
		  action:'create_event_comments'
        };
        return $http.post(vm.eventcommentURL, postData);
	   }
      /***********************************************************/
	  /*create event js code*/
      /***********************************************************/
	  vm.eventURL = 'http://weirddcreations.com/app/events.php';
      function eventCreate(category_id,event_title, location, start_date, end_date, event_image, description, action) {
        var user_id=window.localStorage.getItem("userid");
		var postData = {
		  user_id:user_id,
		  category_id:category_id,
          event_title: event_title,
          location: location,
          start_date: start_date,
          end_date: end_date,
          event_image: event_image,
          description: description,
		  action:'create_events'
        };
        return $http.post(vm.eventURL, postData);
      }
	  /***********************************************************/
	  /*fetch categories dropdown js code*/
      /***********************************************************/
	  vm.categoryURL = 'http://weirddcreations.com/app/categories.php';
      function _getCategoriesDropdown() { 	
	   return $http.get(vm.categoryURL+"?action="+"getCategories");
      };
      return {
        eventCreate: eventCreate,
		getCategoriesDropdown:_getCategoriesDropdown,
		eventComment:eventComment,
		commentByEventId:commentByEventId
      };
    });
