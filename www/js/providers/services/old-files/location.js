angular.module('starter.location', [])

    .controller('locationCtrl', function($scope, $state,serverRepo,serverRepo1, $cordovaGeolocation, $cordovaCamera) {

        $scope.more = function() {
            $state.go('tab.dash')
        }
        $scope.cont = "image here"
        var lat;
        var long;

        $scope.cont = 'img/logo.png'
        $scope.camera = function() {
            // alert("camera")

            /////////////////////camera code/////////////////////////////

            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 1500,
                targetHeight: 1500,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true
            };

            $cordovaCamera.getPicture(options).then(function(imageData) {
                var image = document.getElementById('myImage');
                $scope.image=imageData;
                $scope.cont = "data:image/jpeg;base64," + imageData;

                ///////////////lat long////////


                var options = {
                    timeout: 10000,
                    enableHighAccuracy: true
                };

                $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
                    $scope.lat=position.coords.latitude;
                    $scope.long=position.coords.longitude;

                    var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

                    // var latLng = new google.maps.LatLng(28.7041,77.1025);

                    var mapOptions = {
                        center: latLng,
                        zoom: 15,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
                    google.maps.event.addListenerOnce($scope.map, 'idle', function() {

                        var marker = new google.maps.Marker({
                            map: $scope.map,
                            animation: google.maps.Animation.DROP,
                            position: latLng
                        });

                        var infoWindow = new google.maps.InfoWindow({
                            content: '<img width="100" height="100" src="' + $scope.cont + '"/>'
                        });

                        google.maps.event.addListener(marker, 'click', function() {
                            infoWindow.open($scope.map, marker);
                        });

                    })
                }, function(error) {
                    console.log("Could not get location");
                });

            }, function(err) {
                // error
            });

        }

        // var data = {
        //         "action": "getuserimage",
        //         "id":localStorage.id,
        //         "userid":localStorage.id,
                
        //       }                                                     
        //     console.log("Data---->>" + JSON.stringify(data))
        //     //alert('hhhhhhhhh')
        //     serverRepo1.postServices(data).then(function(res) {
        //       //console.log("6666666===="+JSON.stringify(res))
        //     console.log("6666666===="+JSON.stringify(res))
        //       //alert(data.response)

        //       if(res.data.responce == 'success'){
                
        //               //$scope.data.name= res.data.records.name;
        //               //$scope.data.uname= res.data.records.username;
        //               //$scope.data.website= res.data.records.website;
        //               //$scope.data.bio= res.data.records.bio;
        //               //$scope.data.contact= res.data.records.contactno;
                      
        //             }else{
        //                  var alertPopup = $ionicPopup.alert({
        //                             title: 'LOGIN',
        //                             template: 'Invalid user and password'
        //                });

        //                alertPopup.then(function(res) {

        //                     console.log('Error signup');
        //                });
        //             }
        //     }, function(err) {
        //         console.log("err---->>" + JSON.stringify(err))
        //     })
// $scope.url =[{
//     image: ''
// }];
        //////////////////////save///////
        $scope.save=function(){
        //alert('gvggggggggggggggggggggggg')
         var data = {
                "action":"image",
                "userid":localStorage.id,
                "profile_image" : $scope.image,
                "latitude":$scope.lat,
                "longitude":$scope.long
              }
            console.log("Data---->>" + JSON.stringify(data))

            serverRepo.postServices(data).then(function(res) {
             
            console.log("6666666===="+JSON.stringify(res))
              if(res.data.responce == 'success'){
                //alert('chilllllll')
                      //$scope.url='http://studio-tesseract.co/instagram/upload/'+res.data.records.profile_image;
                     //window.localStorage.setItem("image",'http://studio-tesseract.co/instagram/upload/'+res.data.records.profile_image);
                    }else{
                         var alertPopup = $ionicPopup.alert({
                         title: 'Select Image',
                         template: 'Select Image First'
                       });

                       alertPopup.then(function(res) {

                            console.log('Error signup');
                       });
                    }

             
            }, function(err) {
                console.log("err---->>" + JSON.stringify(err))
            })
        }

    })