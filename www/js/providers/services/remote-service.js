/**
 * Created by Siddhartha Kulshrehstha on 19-03-2018.
 */
angular
  .module("starter.remoteService", [])
  .service("remoteService",
    function ($http) {
      'use strict';
      var vm = this;
      vm.loginURL = 'http://localhost/ems/ems-app/users.php';
      /***********************************************************/
      /***********************************************************/
      function _login(email, password, role, action) {
        var postData = {
          email: email,
          password: password,
		  role: role,
		  action:'login_now'
        };
        return $http.post(vm.loginURL, postData);
      }
      /***********************************************************/
      /***********************************************************/
      return {
        login: _login
      };
    });
