/**
 * Created by Siddhartha Kulshreshtha on 10/4/17.
 */
angular
  .module('starter.studentDetailsService', [])
  .service('studentDetailsService', function ($http) {
    'use strict';
	var vm = this;
	/***********************************************************/
	/*fetch events js code*/
	 /***********************************************************/
	vm.fetchURL = 'http://localhost/ems/ems-app/users.php';
	function _getstudentbyid(studentId){
		return $http.get(vm.fetchURL+"?action="+"getUserDataById"+"&user_id="+studentId);
	};
    return {
	  getstudentbyid:_getstudentbyid
    };
  });
