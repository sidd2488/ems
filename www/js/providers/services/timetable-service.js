/**
 * Created by Siddhartha Kulshreshtha on 10/4/17.
 */
angular
  .module('starter.timetableService', [])
  .service('timetableService', function ($http) {
    'use strict';
	 var vm = this;
	 /***********************************************************/
	/*fetch timetable data js code*/
	 /***********************************************************/
	vm.timetablefetchURL = 'http://localhost/ems/ems-app/users.php';
	function _getClassTimetabled(){
		return $http.get(vm.timetablefetchURL+"?action="+"getClassTimetable");
	};
	
    return {
     getClassTimetabled:_getClassTimetabled
    };
  });
